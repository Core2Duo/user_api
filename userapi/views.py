import jwt
from jwt.exceptions import DecodeError
import json
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.auth.password_validation import validate_password
from django.utils import timezone

from .models import User, UserManager, generate_token
from .utils import JsonResponse, JsonError
import userapi.errors as err


def generate_jwt_token(user_id, token):
    jwt_data = {
        'user_id': user_id,
        'token': token,
    }
    return jwt.encode(jwt_data, settings.JWT_SECRET, algorithm='HS256').decode('utf-8')


class CsrfDisabledView(View):
    """
    This view disables CSRF check for child views.
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CsrfDisabledView, self).dispatch(request, *args, **kwargs)


class ApiAuthView(View):
    """
    Basic View class for API classes that needs auth checks.
    It checks a JWT token and returns 401 error if the credentials are wrong.
    On success returns API response.
    """
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        if not self.check_credentials(request):
            return JsonError('Authentication credentials are wrong', err.AUTH_FAILED, 401)
        else:
            return super(ApiAuthView, self).dispatch(request, *args, **kwargs)

    def check_credentials(self, request):
        token = self.get_auth_token(request)
        if not token:
            return False

        try:
            payload = jwt.decode(token, settings.JWT_SECRET, algorithms=['HS256'])
        except DecodeError:
            return False

        try:
            user_id = payload['user_id']
            token = payload['token']
        except KeyError:
            return False

        try:
            user = User.objects.get(pk=user_id, token=token)
        except ObjectDoesNotExist:
            return False

        request.user = user
        return True

    def get_auth_token(self, request):
        auth = request.META.get('HTTP_AUTHORIZATION', False)
        if not auth:
            return False

        prefix = 'Bearer '
        if not auth.lower().startswith(prefix.lower()):
            return False

        token = auth[len(prefix):]
        return token


class UserRegisterAnonymousView(CsrfDisabledView):
    """
    Registers a new anonymous user and returns a token which should be stored on a frontend
    and used for other methods.
    """
    def post(self, request):
        user = User()
        user.set_unusable_password()
        user.save()

        jwt_token = generate_jwt_token(user.pk, user.token)
        return JsonResponse({
            'user_id': user.pk,
            'token': jwt_token,
        }, 201)


class UserRegisterView(ApiAuthView):
    """
    Registers an anonymous user as a normal user
    """
    def post(self, request):
        try:
            body = json.loads(request.body.decode('utf-8'))
            email = body['email']
            password = body['password']
        except ValueError:
            return JsonError('The request must be a correct JSON', err.WRONG_DATA)
        except KeyError:
            return JsonError('You must provide email and password', err.WRONG_DATA)

        if not request.user.anonymous:
            return JsonError('The user is already registered', err.REGISTER_ALREADY_REGISTERED)

        user = request.user
        user.email = UserManager.normalize_email(email)
        user.set_password(password)
        user.anonymous = False
        user.registered = timezone.now()

        try:
            validate_password(password, user)
        except ValidationError as e:
            return JsonError(' '.join(e), err.FIELD_BAD_PASSWORD)

        try:
            user.clean_fields()
        except ValidationError as e:
            return JsonError(' '.join(e.message_dict['email']), err.FIELD_BAD_EMAIL)

        try:
            user.validate_unique()
        except ValidationError as e:
            return JsonError(' '.join(e.message_dict['email']), err.FIELD_DUPLICATE_EMAIL)

        user.save()
        return JsonResponse({})


class UserSignInView(CsrfDisabledView):
    def post(self, request):
        try:
            body = json.loads(request.body.decode('utf-8'))
            email = body['email']
            password = body['password']
        except ValueError:
            return JsonError('The request must be a correct JSON', err.WRONG_DATA)
        except KeyError:
            return JsonError('You must provide email and password', err.WRONG_DATA)

        try:
            user = User.objects.get(email=email)
        except ObjectDoesNotExist:
            return JsonError('Incorrect email or password', err.LOGIN_BAD_CREDENTIALS)

        if not user.check_password(password):
            return JsonError('Incorrect email or password', err.LOGIN_BAD_CREDENTIALS)

        if user.anonymous:
            return JsonError('The user is anonymous', err.USER_ANONYMOUS)

        return JsonResponse({
            'user_id': user.pk,
            'token': generate_jwt_token(user.pk, user.token),
        })


class UserSignOutView(ApiAuthView):
    """
    This API method should be called only if you want to sign out a user from EVERY frontend,
    because it changes user.token what makes all of user's JWT tokens incorrect.
    If you want to sign out a user from a single frontend, just delete a JWT token on this frontend.
    """
    def post(self, request):
        user = request.user
        if user.anonymous:
            return JsonError('The user is anonymous', err.USER_ANONYMOUS)

        user.token = generate_token()
        user.save()

        return JsonResponse({})


class UserChangeEmailView(ApiAuthView):
    def post(self, request):
        try:
            body = json.loads(request.body.decode('utf-8'))
            email = body['email']
        except ValueError:
            return JsonError('The request must be a correct JSON', err.WRONG_DATA)
        except KeyError:
            return JsonError('You must provide email', err.WRONG_DATA)

        user = request.user
        user.email = email

        if user.anonymous:
            return JsonError('The user is anonymous', err.USER_ANONYMOUS)

        try:
            user.clean_fields()
        except ValidationError as e:
            return JsonError(' '.join(e.message_dict['email']), err.FIELD_BAD_EMAIL)

        try:
            user.validate_unique()
        except ValidationError as e:
            return JsonError(' '.join(e.message_dict['email']), err.FIELD_DUPLICATE_EMAIL)

        user.save()

        return JsonResponse({})


class UserChangePasswordView(ApiAuthView):
    def post(self, request):
        try:
            body = json.loads(request.body.decode('utf-8'))
            password = body['password']
            old_password = body['old_password']
        except ValueError:
            return JsonError('The request must be a correct JSON', err.WRONG_DATA)
        except KeyError:
            return JsonError('You must provide password and old password', err.WRONG_DATA)

        user = request.user

        if user.anonymous:
            return JsonError('The user is anonymous', err.USER_ANONYMOUS)

        if not user.check_password(old_password):
            return JsonError('The old password is wrong', err.FIELD_BAD_OLD_PASSWORD)

        try:
            validate_password(password, user)
        except ValidationError as e:
            return JsonError(' '.join(e), err.FIELD_BAD_PASSWORD)

        user.set_password(password)
        user.save()

        return JsonResponse({})

