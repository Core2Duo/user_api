import json
from django.test import TestCase

from .models import User, generate_email
from .views import generate_jwt_token
from userapi import errors as err


def setUpModule():
    pass


class ApiViewsTest(TestCase):
    def setUp(self):
        User.objects.create(email=generate_email())
        User.objects.create(email=generate_email())
        User.objects.create(email='unique@test.com', anonymous=False)
        u = User(email='can_login@test.com', anonymous=False)
        u.set_password('123qwe123')
        u.save()
        u = User(email='cannot_login@test.com')
        u.set_password('123qwe123')
        u.save()

    def tearDown(self):
        pass

    def api_request(self, url, data=None, token=None):
        kwargs = {
            'content_type': 'application/json',
        }
        
        if token:
            kwargs['HTTP_AUTHORIZATION'] = 'Bearer %s' % token

        if not data:
            data = {}
        data = json.dumps(data)

        response = self.client.post(url, data, **kwargs)

        return response, response.json()

    def testUserRegisterAnonymousView(self):
        response, body = self.api_request('/api/v1/user/register-anonymous/')

        self.assertEqual(response.status_code, 201)
        self.assertFalse(body['error'])
        self.assertIn('token', body)
        self.assertIn('user_id', body)

    def testAuthView(self):
        response, body = self.api_request('/api/v1/user/register/')
        self.assertEqual(response.status_code, 401)

    def testAuthView2(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)

        response, body = self.api_request('/api/v1/user/register/', token=token)

        self.assertEqual(response.status_code, 400)

    def testUserRegisterView(self):
        response, body = self.api_request('/api/v1/user/register/', token='qweqwe')

        self.assertEqual(response.status_code, 401)

    def testUserRegisterView2(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)

        response, body = self.api_request('/api/v1/user/register/', token=token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.WRONG_DATA)

    def testUserRegisterView3(self):
        user = User.objects.filter(anonymous=False)[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'test@test.com',
            'password': '123',
        }

        response, body = self.api_request('/api/v1/user/register/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.REGISTER_ALREADY_REGISTERED)

    def testUserRegisterView4(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': '',
        }

        response, body = self.api_request('/api/v1/user/register/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.WRONG_DATA)

    def testUserRegisterView5(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'test@test.com',
            'password': '123',
        }

        response, body = self.api_request('/api/v1/user/register/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_BAD_PASSWORD)

    def testUserRegisterView6(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'testtest.com',
            'password': '12asdasdq3',
        }

        response, body = self.api_request('/api/v1/user/register/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_BAD_EMAIL)

    def testUserRegisterView7(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'unique@test.com',
            'password': '12asdasdq3',
        }

        response, body = self.api_request('/api/v1/user/register/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_DUPLICATE_EMAIL)

    def testUserRegisterView8(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'not_unique@test.com',
            'password': '12asdasdq3',
        }

        response, body = self.api_request('/api/v1/user/register/', data, token)

        self.assertEqual(response.status_code, 200)

    def testUserSignInView(self):
        data = {}

        response, body = self.api_request('/api/v1/user/sign-in/', data)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.WRONG_DATA)

    def testUserSignInView2(self):
        data = {
            'email': 'not-existing@test.com',
            'password': '123d123123',
        }

        response, body = self.api_request('/api/v1/user/sign-in/', data)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.LOGIN_BAD_CREDENTIALS)

    def testUserSignInView3(self):
        data = {
            'email': 'unique@test.com',
            'password': 'bad_password',
        }

        response, body = self.api_request('/api/v1/user/sign-in/', data)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.LOGIN_BAD_CREDENTIALS)

    def testUserSignInView4(self):
        data = {
            'email': 'can_login@test.com',
            'password': '123qwe123',
        }

        response, body = self.api_request('/api/v1/user/sign-in/', data)

        self.assertEqual(response.status_code, 200)

    def testUserSignInView5(self):
        data = {
            'email': 'cannot_login@test.com',
            'password': '123qwe123',
        }

        response, body = self.api_request('/api/v1/user/sign-in/', data)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.USER_ANONYMOUS)

    def testUserSignOut(self):
        user = User.objects.all()[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {}

        response, body = self.api_request('/api/v1/user/sign-out/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.USER_ANONYMOUS)

    def testUserSignOut2(self):
        user = User.objects.filter(anonymous=False)[0]
        token = generate_jwt_token(user.pk, user.token)
        pk = user.pk
        old_token = user.token
        data = {}

        response, body = self.api_request('/api/v1/user/sign-out/', data, token)

        user = User.objects.get(pk=pk)

        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(user.token, old_token)

    def testUserChangeEmail(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {}

        response, body = self.api_request('/api/v1/user/change-email/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.WRONG_DATA)

    def testUserChangeEmail2(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'bademail',
        }

        response, body = self.api_request('/api/v1/user/change-email/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_BAD_EMAIL)

    def testUserChangeEmail3(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'unique@test.com',
        }

        response, body = self.api_request('/api/v1/user/change-email/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_DUPLICATE_EMAIL)

    def testUserChangeEmail4(self):
        user = User.objects.filter(anonymous=True)[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'not_unique@test.com',
        }

        response, body = self.api_request('/api/v1/user/change-email/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.USER_ANONYMOUS)

    def testUserChangeEmail5(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'email': 'not_unique@test.com',
        }

        response, body = self.api_request('/api/v1/user/change-email/', data, token)

        User.objects.get(email='not_unique@test.com')
        self.assertEqual(response.status_code, 200)

    def testUserChangePasswordView(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {}

        response, body = self.api_request('/api/v1/user/change-password/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.WRONG_DATA)

    def testUserChangePasswordView2(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'password': '123',
            'old_password': 'wrong_password',
        }

        response, body = self.api_request('/api/v1/user/change-password/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_BAD_OLD_PASSWORD)

    def testUserChangePasswordView3(self):
        user = User.objects.filter(anonymous=True)[0]
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'password': '123',
            'old_password': 'wrong_password',
        }

        response, body = self.api_request('/api/v1/user/change-password/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.USER_ANONYMOUS)

    def testUserChangePasswordView4(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'password': 'bad',
            'old_password': '123qwe123',
        }

        response, body = self.api_request('/api/v1/user/change-password/', data, token)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(body['error_code'], err.FIELD_BAD_PASSWORD)

    def testUserChangePasswordView5(self):
        user = User.objects.get(email='can_login@test.com')
        token = generate_jwt_token(user.pk, user.token)
        data = {
            'password': 'qweasdzxc',
            'old_password': '123qwe123',
        }

        response, body = self.api_request('/api/v1/user/change-password/', data, token)

        user = User.objects.get(email='can_login@test.com')
        self.assertTrue(user.check_password('qweasdzxc'))
        self.assertEqual(response.status_code, 200)
