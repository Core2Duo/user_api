from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.utils import timezone
from random import choice
import string
from uuid import uuid4


def generate_token():
    return ''.join([choice(string.digits + string.ascii_letters) for _ in range(32)])


def generate_email():
    return '%s@userapi.local' % uuid4()


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('You should enter user\'s email')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password):
        if not email:
            raise ValueError('You should enter user\'s email')

        user = self.model(email=self.normalize_email(email), is_superuser=True, is_staff=True)
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = 'email'

    objects = UserManager()

    email = models.EmailField('email', unique=True, default=generate_email)

    is_staff = models.BooleanField('staff status', default=False,
                                   help_text='Designates whether the user can log into this admin site.')
    is_active = models.BooleanField('активен', default=True,
                                    help_text='Designates whether this user should be treated as '
                                              'active. Unselect this instead of deleting accounts.')

    joined = models.DateTimeField(default=timezone.now)
    registered = models.DateTimeField(blank=True, null=True)

    anonymous = models.BooleanField(default=True)
    token = models.CharField(max_length=32, default=generate_token)

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email
