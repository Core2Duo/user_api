from django.http import JsonResponse as _JsonResponse


class JsonError(_JsonResponse):
    def __init__(self, message, code, status_code=400, **kwargs):
        data = {
            'error': True,
            'error_code': code,
            'error_message': message,
        }

        super(JsonError, self).__init__(data, status=status_code, **kwargs)


class JsonResponse(_JsonResponse):
    def __init__(self, data, code=200, **kwargs):
        if 'error' not in data:
            data['error'] = False

        super(JsonResponse, self).__init__(data, status=code, **kwargs)
