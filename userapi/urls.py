from django.conf.urls import url
from .views import UserRegisterView, UserRegisterAnonymousView, UserChangeEmailView, UserSignInView, \
    UserSignOutView, UserChangePasswordView

urlpatterns = [
    url(r'^user/register-anonymous/$', UserRegisterAnonymousView.as_view(), name='user-register-anonymous'),
    url(r'^user/register/$', UserRegisterView.as_view(), name='user-register'),
    url(r'^user/sign-in/$', UserSignInView.as_view(), name='user-sign-in'),
    url(r'^user/sign-out/$', UserSignOutView.as_view(), name='user-sign-out'),
    url(r'^user/change-email/$', UserChangeEmailView.as_view(), name='user-change-email'),
    url(r'^user/change-password/$', UserChangePasswordView.as_view(), name='user-change-password'),
]
